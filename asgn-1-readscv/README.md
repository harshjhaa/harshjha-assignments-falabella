# hjha-assgn-1-readscv
Assignment 1: Reading the CSV file and doing operations in it.

# Getting started
This project allows you to-
    - read and write into the CSV file.
    - do operation like extracting a selected set of data out of it.
    - produce the operated CSV file.
    - plot a bar-graph for the extracted data.

Below are the steps required to run the project-
    - Install the required dependencies using command: npm install
    - Run the project using command: node index.js

# Tech Stack Used and Libraries Used
    - JavaScript
    - NodeJs

# Node packages used
    - csv-parser: library for reading CSV files
    - nodeplotlib: library for producing graph 
