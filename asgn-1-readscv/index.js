const fs = require('fs');
const csv = require('csv-parser');
const plotlib = require('nodeplotlib');

const data = [];
const graphArr = [
    {
        x: [],
        y: [],
        type: 'bar'
    }
];
const asean_countries = 'ASEAN_COUNTRIES';
const ASEAN_COUNTRIES = [
    "Cambodia"
    , "Indonesia"
    , "Laos"
    , "Malaysia"
    , "Myanmar"
    , "Philippines"
    , "Singapore"
    , "Thailand"
    , "Viet Nam"
    , "Brunei Darussalam"

];
const saarc_countries = 'SAARC_COUNTRIES';
const SAARC_COUNTRIES = [
    "Afghanistan",
    "Bangladesh",
    "Bhutan",
    "India",
    "Maldives",
    "Nepal",
    "Pakistan",
    "Sri Lanka"
];

fs.createReadStream('data.csv')
    .pipe(csv())
    .on('data', function (row) {
        const value = {
            country_name: row.Region,
            country_code: row.Country_Code,
            year: row.Year,
            population: row.Population,
        }
        data.push(value)
    })
    .on('end', function () {
        writeToCSVFile(data);
    })

function writeToCSVFile(data) {
    const filename = 'output.csv';
    fs.writeFile(filename,
        /*
            second arg can be toggled between - "India", saarc_countries, asean_countries
        */
        extractAsCSV(data, "India"),
        // extractAsCSV(data, asean_countries),
        // extractAsCSV(data, saarc_countries),
        err => {
            if (err) {
                console.log('Error writing to csv file', err);
            } else {
                console.log(`File saved as: ${filename}`);
            }
        });
}

function pushToGraphArr(xVal, yVal) {
    graphArr[0].x.push(xVal);
    graphArr[0].y.push(yVal);
}

function extractAsCSV(data, val) {
    const header = ["country_name, country_code, year, population"];
    let tmpRowArr = [];

    if (val === "India") {
        tmpRowArr = data.filter(row => {
            if (row.country_name === val) {
                pushToGraphArr(row.year, row.population);
                return row;
            }
        });
    } else if (val === asean_countries) {
        data.forEach(row => {
            if (ASEAN_COUNTRIES.indexOf(row.country_name) >= 0) {
                tmpRowArr.push(row);
                if (row.year === "2014")
                    pushToGraphArr(row.country_name, row.population);
            }
        })
    } else if (val === saarc_countries) {
        let res = {};
        data.forEach(row => {
            if (SAARC_COUNTRIES.indexOf(row.country_name) >= 0) {
                tmpRowArr.push(row);
                res[row.year] === undefined ? res[row.year] = 0 : res[row.year] += parseInt(row.population);
            }
        });
        for (const key in res) {
            pushToGraphArr(key, res[key]);
        }
    }

    const rows = tmpRowArr.map(row =>
        `${row.country_name}, ${row.country_code}, ${row.year}, ${row.population}`
    );

    // console.log(rows);
    // console.log(graphArr);

    /* for ploting the graph */
    plotlib.plot(graphArr);

    return header.concat(rows).join("\n");
}